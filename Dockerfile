ARG CONTAINER_PROXY
ARG FROM_IMAGE="ubuntu:20.04"
FROM ${CONTAINER_PROXY}${FROM_IMAGE}

RUN apt-get update -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ca-certificates \
        wget \
    && rm -rf /var/lib/apt/lists/*

ARG CONTAINER_DOCKER_MACHINE_ARCH="x86_64"

ARG VERIBLE_VERSION="v0.0-3104-g75f24062"
ENV VERIBLE_VERSION="${VERIBLE_VERSION}"

RUN . /etc/os-release \
    && export OS="${NAME}-${VERSION_ID}-${VERSION_CODENAME}" \
    && export ARCH="${CONTAINER_DOCKER_MACHINE_ARCH}" \
    && echo "OS: $OS" \
    && echo "ARCH: $ARCH" \
    && export VERIBLE_TARBALL="verible-${VERIBLE_VERSION}-${OS}-${ARCH}.tar.gz" \
    && export VERIBLE_DIR="verible-${VERIBLE_VERSION}" \
    && wget --progress=dot:giga "https://github.com/chipsalliance/verible/releases/download/${VERIBLE_VERSION}/${VERIBLE_TARBALL}" \
    && tar xf "${VERIBLE_TARBALL}" \
    && install ${VERIBLE_DIR}/bin/* /usr/local/bin/ \
    && rm -rf "${VERIBLE_DIR}" "${VERIBLE_TARBALL}" \
    && verible-verilog-format --version
