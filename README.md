# Verible container

<!-- BADGIE TIME -->

[![brettops container](https://img.shields.io/badge/brettops-container-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://gitlab.com/brettops/containers/verible/badges/main/pipeline.svg)](https://gitlab.com/brettops/containers/verible/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

This container provides the [Verible](https://github.com/chipsalliance/verible)
suite of developer tools for Verilog.

## Usage

New Verible versions are released almost daily, so `latest` is recommended:

```
docker pull registry.gitlab.com/brettops/containers/verible
```

Images are rebuilt weekly, so the Verible version will be no
more than a week old.

This is a multiarch image and `x86_64` and `aarch64` versions of this image are
available.

## Build locally

To build the image with the default configuration:

```bash
docker build -t verible .
```

Grab a shell in the image:

```bash
docker run --rm -it -v `pwd`:/code -w /code verible bash
```
